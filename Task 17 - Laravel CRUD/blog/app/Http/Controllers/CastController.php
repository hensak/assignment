<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
        ]);
        $cast = new Cast;
        $cast->nama = $request->name;
        $cast->umur = $request->age;
        $cast->bio = $request->bio != null ? $request->bio : " ";
        $cast->save();

        return redirect('/cast');
    }

    public function index() {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id) {
        $cast = Cast::where('id', $cast_id)->first();
        return view('cast.show' ,compact('cast'));
    }

    public function edit($cast_id) {
        $cast = Cast::where('id', $cast_id)->first();
        return view('cast.edit' ,compact('cast'));
    }

    public function update(Request $request, $cast_id) {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
        ]);
        $cast = Cast::find($cast_id);
        $cast->nama = $request->name;
        $cast->umur = $request->age;
        $cast->bio = $request->bio != null ? $request->bio : " ";
        $cast->save();

        return redirect('/cast');
    }

    public function destroy($cast_id) {
        $cast = Cast::find($cast_id);
        $cast->delete();

        return redirect('/cast');
    }
}
