@extends('layout.master')

@section('judul')
Welcome Page
@endsection

@section('content')
<h1>SELAMAT DATANG, {{$fname}} {{$lname}}!</h1>
<h3>Terima kasih telah bergabung di website kami. Media belajar kita bersama!</h3>
@endsection