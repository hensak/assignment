@extends('layout.master')

@section('judul')
Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Name</label>
      <input type="text" name="name" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('name')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Age</label>
      <input type="text" name="age" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('age')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" value="{{$cast->bio}}" class="form-control" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection