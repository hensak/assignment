<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'App\Http\Controllers\IndexController@index');
Route::get('/register', 'App\Http\Controllers\AuthController@form');
Route::post('/welcome', 'App\Http\Controllers\AuthController@welcome');
Route::get('/data-table', function() {
    return view('table.data-table');
});

//CRUD
//Create
Route::get('/cast/create', 'App\Http\Controllers\CastController@create');        //menampilkan form untuk membuat data pemain film baru
Route::post('/cast', 'App\Http\Controllers\CastController@store');               //menyimpan data baru ke tabel Cast
//Read
Route::get('/cast', 'App\Http\Controllers\CastController@index');                //menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
Route::get('/cast/{cast_id}', 'App\Http\Controllers\CastController@show');       //menampilkan detail data pemain film dengan id tertentu
//Update
Route::get('/cast/{cast_id}/edit', 'App\Http\Controllers\CastController@edit');  //menampilkan form untuk edit pemain film dengan id tertentu
Route::put('/cast/{cast_id}', 'App\Http\Controllers\CastController@update');     //menyimpan perubahan data pemain film (update) untuk id tertentu
//Delete
Route::delete('/cast/{cast_id}', 'App\Http\Controllers\CastController@destroy');  //menghapus data pemain film dengan id tertentu