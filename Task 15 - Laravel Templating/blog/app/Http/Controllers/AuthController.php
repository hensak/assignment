<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('page.form');
    }
    public function welcome(Request $request) {
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('page.welcome', compact('fname', 'lname'));
    }
}
