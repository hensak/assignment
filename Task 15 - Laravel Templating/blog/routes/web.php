<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'App\Http\Controllers\IndexController@index');

Route::get('/register', 'App\Http\Controllers\AuthController@form');

Route::get('/welcome', 'App\Http\Controllers\AuthController@welcome');

Route::get('/data-table', function() {
    return view('table.data-table');
});