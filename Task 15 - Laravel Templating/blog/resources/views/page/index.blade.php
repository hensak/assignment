@extends('layout.master')

@section('judul')
Index Page
@endsection

@section('content')
<h1>Media Online</h1>
<h2>Sosial Media Developer</h2>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<h3>Benefit join di Media Online</h3>
<ul>
    <li>Mendapat motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h3>Cara Bergabung ke Media Online</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
@endsection