<!DOCTYPE html>
<html>
<body>

<form action="/welcome" method="get">   
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>

    @csrf
    <p>First name :</p>
    <input type="text" name='fname'>

    <p>Last name :</p>
    <input type="text" name='lname'>

    <p>Gender :</p>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female

    <p>Nationality :</p>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="others">Others</option>
    </select>
    <p>Language Spoken :</p>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other

    <p>Bio :</p>
    <textarea name="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>

</body>
</html>