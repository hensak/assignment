@extends('layout.master')

@section('judul')
Detail Cast
@endsection

@section('content')
<h3>{{$cast->nama}}</h3>
<p>{{$cast->umur}} years old</p>
<p>Biodata: {{$cast->bio}}</p>
@endsection 