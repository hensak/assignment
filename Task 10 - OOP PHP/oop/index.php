<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <?php
    require ('animal.php');
    require ('ape.php');
    require ('frog.php');

    function redundant($animal) {
        $animal->name();
        $animal->legs();
        $animal->cold_blooded();
    }

    $sheep = new Animal("Shaun");
    redundant($sheep);
    echo "<br>";
     
    $kodok = new Frog("Buduk");
    redundant($kodok);
    $kodok->jump();
    echo "<br>";

    $sungokong = new Ape("Kera Sakti");
    redundant($sungokong);
    $sungokong->yell();
    echo "<br>";
    ?>
</body>

</html>