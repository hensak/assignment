<?php
require_once ('animal.php');

class Frog extends Animal {
    public $xname;
    public $xjump = "Hop Hop";

    function __construct($xname) {
        $this->xname = $xname;
    }

    public function jump() {
        echo "Jump : {$this->xjump}<br>";
    }
}
?>