<?php
require_once ('animal.php');

class Ape extends Animal {
    public $xname;
    public $xyell = "Auooo";
    public $xlegs = 2;

    function __construct($xname) {
        $this->xname = $xname;
    }

    public function yell() {
        echo "Yell : {$this->xyell}<br>";
    }
}
?>