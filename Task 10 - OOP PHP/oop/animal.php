<?php
class Animal {
    public $xname;
    public $xlegs = 4;
    public $xcold_blood = "No";
    
    function __construct($xname) {
        $this->xname = $xname;
    }

    public function name() {
        echo "Name : {$this->xname}<br>";
    }
    public function legs() {
        echo "Legs : {$this->xlegs}<br>";
    }
    public function cold_blooded() {
        echo "Cold Blooded : {$this->xcold_blood}<br>";
    }
}
?>