@extends('layout.master')

@section('judul')
Add New Cast
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label>Name</label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Age</label>
      <input type="text" name="age" class="form-control">
    </div>
    @error('age')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" class="form-control" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection