@extends('layout.master')

@section('judul')
List of Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-secondary nb-3">Add</a>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item->nama}}</td>
          <td>{{$item->umur}}</td>
          <td>{{$item->bio}}</td>
          <td>
            <form action="/cast/{{$item->id}}" method="post">
              <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Details</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
          </td>
        </tr>
      @empty
        <h4>Data not found</h4>
      @endforelse
    </tbody>
</table>
@endsection