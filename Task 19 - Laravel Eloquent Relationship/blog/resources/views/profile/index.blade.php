@extends('layout.master')

@section('judul')
Edit Profile
@endsection

@section('content')
<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Name</label>
      <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
    </div>
    <div class="form-group">
      <label>Age</label>
      <input type="number" name="age" value="{{$profile->umur}}" class="form-control">
    </div>
    @error('age')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Address</label>
      <input type="text" name="address" value="{{$profile->alamat}}" class="form-control">
    </div>
    @error('address')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" value="{{$profile->bio}}" class="form-control" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection