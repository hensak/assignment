@extends('layout.master')

@section('judul')
List of Movie
@endsection

@section('content')
<a href="/movie/create" class="btn btn-secondary nb-3">Add</a>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Year</th>
        <th scope="col">Genre</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($movie as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item->judul}}</td>
          <td>{{$item->tahun}}</td>
          <td>{{$item->genre->nama}}</td>
          <td>
            <form action="/movie/{{$item->id}}" method="post">
              <a href="/movie/{{$item->id}}" class="btn btn-info btn-sm">Details</a>
              <a href="/movie/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
          </td>
        </tr>
      @empty
        <h4>Data not found</h4>
      @endforelse
    </tbody>
</table>
@endsection