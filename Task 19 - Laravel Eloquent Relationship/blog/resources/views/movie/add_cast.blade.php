@extends('layout.master')

@section('judul')
Add Cast to {{$movie->judul}}
@endsection

@section('content')
<form action="/movie/{{$movie->id}}/add_cast/save" method="post">
  @csrf
  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="">Role and Cast</span>
    </div>
    <input type="text" name="role" value="Main Character" class="form-control">
    <select name="cast" class="custom-select">
      @forelse ($cast as $key => $item)
        <option>{{$item->nama}}</option>
      @empty
      @endforelse
    </select>
    <div class="input-group-append">
      <button type="submit" class="btn btn-outline-secondary">Add</button>
    </div>
  </div>

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Role</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($role as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item->cast->nama}}</td>
          <td>{{$item->nama}}</td>
        </tr>
      @empty
        <h4>Data not found</h4>
      @endforelse
    </tbody>
  </table>
</form>
@endsection