@extends('layout.master')

@section('judul')
Detail Movie
@endsection

@section('content')
<form action="/movie/{{$movie->id}}/comment" method="post">
    @csrf
    @method('put')

    <h3>{{$movie->judul}}</h3>
    <label>Genre:</label>
    <p>{{$movie->genre->nama}}</p>
    <label>Release year:</label>
    <p>{{$movie->tahun}}</p>
    <label>Synopsis:</label>
    <p>{{$movie->ringkasan}}</p>

    <div>    
        <label>Cast List:</label>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Role</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($role as $key => $item)
              <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->cast->nama}}</td>
                <td>{{$item->nama}}</td>
              </tr>
            @empty
              <h4>Data not found</h4>
            @endforelse
          </tbody>
        </table>
      </div>

    <div class="card">
        <Label>Comment:</Label>

        @forelse ($comment as $key => $item)
            <div class="card">
            <div class="card-body">
            <h6 class="card-title">{{$item->user->name}}</h6>
            <p class="card-text">{{$item->content}}</p>
            </div>
            </div>
        @empty
        @endforelse

        <div class="card-body">
            <div class="form-group">
                <textarea name="comment" class="form-control" rows="3"></textarea>
            </div>
            @error('comment')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
            
            <button type="submit" class="btn btn-primary">Comment</button>
        </div>
    </div>
</form>
@endsection 