@extends('layout.master')

@section('judul')
Add New Movie
@endsection

@section('content')
<form action="/movie" method="post">
    @csrf
    <div class="form-group">
      <label>Title</label>
      <input type="text" name="title" class="form-control">
    </div>
    @error('title')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Genre</label>
      <input type="text" name="genre" class="form-control">
    </div>
    @error('genre')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Release year</label>
      <input type="number" name="year" class="form-control">
    </div>
    @error('year')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Synopsis</label>
      <textarea name="synopsis" class="form-control" rows="10"></textarea>
    </div>
    @error('synopsis')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection