@extends('layout.master')

@section('judul')
Edit Movie
@endsection

@section('content')
<form action="/movie/{{$movie->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Title</label>
      <input type="text" name="title" value="{{$movie->judul}}" class="form-control">
    </div>
    @error('title')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Genre</label>
      <input type="text" name="genre" value="{{$movie->genre->nama}}" class="form-control">
    </div>
    @error('genre')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Release year</label>
      <input type="number" name="year" value="{{$movie->tahun}}" class="form-control">
    </div>
    @error('year')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Synopsis</label>
      <textarea name="synopsis" class="form-control" value="{{$movie->ringkasan}}" rows="10"></textarea>
    </div>
    @error('synopsis')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div>
    <a href="/movie/{{$movie->id}}/add_cast" class="btn btn-primary">Edit Cast</a>
    <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection