<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Movie;
use App\Models\Cast;
use App\Models\Role;
use App\Models\Genre;
use App\Models\Comment;
use App\Models\UserDumb;

class MovieController extends Controller
{
    public function create() {
        return view('movie.create');
    }

    public function store(Request $request) {
        $request->validate([
            'title' => 'required',
            'synopsis' => 'required',
            'year' => 'required',
            'genre' => 'required'
        ]);

        $genre = Genre::where('nama', $request->genre)->first();
        if($genre == null) {
            Genre::create([
                'nama' => $request->genre,
            ]);
        }
        $genre = Genre::where('nama', $request->genre)->first();

        Movie::create([
            'judul' => $request->title,
            'ringkasan' => $request->synopsis,
            'tahun' => $request->year,
            'poster' => " ",
            'genre_id' => $genre->id,
        ]);

        return redirect('/movie');
    }

    public function index() {
        $movie = Movie::all();
        return view('movie.index', compact('movie'));
    }

    public function show($movie_id) {
        $movie = Movie::where('id', $movie_id)->first();
        $role = Role::where('film_id', $movie_id)->get();
        $comment = Comment::where('film_id', $movie_id)->get();
        return view('movie.show', compact('movie','role','comment'));
    }

    public function edit($movie_id) {
        $cast = Cast::all();
        $movie = Movie::where('id', $movie_id)->first();
        return view('movie.edit', compact('movie','cast'));
    }

    public function update(Request $request, $movie_id) {
        $request->validate([
            'title' => 'required',
            'synopsis' => 'required',
            'year' => 'required',
            'genre' => 'required'
        ]);

        $genre = Genre::where('nama', $request->genre)->first();
        if($genre == null) {
            Genre::create([
                'nama' => $request->genre,
            ]);
        }
        $genre = Genre::where('nama', $request->genre)->first();

        $movie = Movie::find($movie_id);
        $movie->judul = $request->title;
        $movie->ringkasan = $request->synopsis;
        $movie->tahun = $request->year;
        $movie->poster = " ";
        $movie->genre_id = $genre->id;
        $movie->save();

        return redirect('/movie');
    }

    public function destroy($movie_id) {
        $movie = Movie::find($movie_id);
        $movie->delete();

        return redirect('/movie');
    }

    public function comment(Request $request, $movie_id) {
        $request->validate([
            'comment' => 'required',
        ]);

        $userdumb = UserDumb::where('email', Auth::user()->email)->first();

        Comment::create([
            'user_id' => $userdumb->id,
            'film_id' => $movie_id,
            'content' => $request->comment,
            'point' => 0,
        ]);

        $string = "/movie/{$movie_id}";
        return redirect($string);
    }

    public function add_cast($movie_id) {
        $cast = Cast::all();
        $role = Role::where('film_id', $movie_id)->get();
        $movie = Movie::where('id', $movie_id)->first();
        return view('movie.add_cast', compact('movie','cast','role'));
    }

    public function save_cast(Request $request, $movie_id) {
        $request->validate([
            'cast' => 'required',
            'role' => 'required',
        ]);

        $cast = Cast::where('nama', $request->cast)->first();

        Role::create([
            'nama' => $request->role,
            'film_id' => $movie_id,
            'cast_id' => $cast->id,
        ]);

        $string = "/movie/{$movie_id}/add_cast";
        return redirect($string);
    }
}
