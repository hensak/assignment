<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;
use App\Models\UserDumb;

class ProfileController extends Controller
{
    public function index() {
        $userdumb = UserDumb::where('email', Auth::user()->email)->first();
        $profile = Profile::where('user_id', $userdumb->id)->first();

        return view('profile.index', compact('profile'));
    } 

    public function update(Request $request, $profile_id) {
        $request->validate([
            'age' => 'required',
            'address' => 'required',
        ]);
        $profile = Profile::find($profile_id);
        $profile->umur = $request->age;
        $profile->alamat = $request->address;
        $profile->bio = $request->bio != null ? $request->bio : " ";
        $profile->save();

        return redirect('/');
    }
}
