<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDumb extends Model
{
    // use HasFactory;
    protected $table = 'user';
    protected $fillable = ['name','email','password'];

    public function profile() {
        return $this->hasOne('App\Models\Profile');
    }

    public function comment() {
        return $this->hasMany('App\Models\Comment');
    }
}
