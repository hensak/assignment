<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    // use HasFactory;
    protected $table = 'film';
    protected $fillable = ['judul','ringkasan','tahun','poster','genre_id'];

    public function genre() {
        return $this->belongsTo(('App\Models\Genre'));
    }

    public function role() {
        return $this->hasMany(('App\Models\Role'));
    }

    public function comment() {
        return $this->hasMany('App\Models\Comment');
    }
}
