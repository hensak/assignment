<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // use HasFactory;
    protected $table = 'peran';
    protected $fillable = ['nama','film_id','cast_id'];

    public function movie() {
        return $this->belongsTo(('App\Models\Movie'));
    }

    public function cast() {
        return $this->belongsTo(('App\Models\Cast'));
    }
}
