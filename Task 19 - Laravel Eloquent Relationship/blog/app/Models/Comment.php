<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // use HasFactory;
    protected $table = 'kritik';
    protected $fillable = ['user_id','film_id','content','point'];

    public function user() {
        return $this->belongsTo(('App\Models\UserDumb'));
    }

    public function movie() {
        return $this->belongsTo(('App\Models\Movie'));
    }
}
